package com.example.myapplication;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;

public class IMCActivity extends AppCompatActivity {

    private TextView lblResultado;
    private EditText txtPeso, txtAltura;
    private Button btnCalcular, btnLimpiar, btnCerrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_imcactivity);
        iniciarComponentes();
    }

    public void iniciarComponentes() {
        lblResultado = findViewById(R.id.lblResultado);
        txtPeso = findViewById(R.id.txtPeso);
        txtAltura = findViewById(R.id.txtAltura);
        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnCerrar = findViewById(R.id.btnRegresar);

        // Evento para limpiar
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtAltura.setText("");
                txtPeso.setText("");
                lblResultado.setText("IMC es: "); // Volver al origen del texto
            }
        });

        // Evento para cerrar
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        // Evento para calcular el IMC
        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calcularIMC(); // Llamada a la función para calcular el IMC
            }
        });
    }

    public void calcularIMC() {
        String peso = txtPeso.getText().toString();
        String altura = txtAltura.getText().toString();
        if (peso.isEmpty() || altura.isEmpty()) {
            Toast.makeText(getApplicationContext(), "¡Por favor rellene los datos faltan!", Toast.LENGTH_SHORT).show();
        } else {
            try {
                if (!altura.contains(".")) {
                    Toast.makeText(getApplicationContext(), "Debe estar en metros la altura", Toast.LENGTH_SHORT).show();
                    return;
                }
                double kg = Double.parseDouble(peso);
                double h = Double.parseDouble(altura);
                double imc = kg / Math.pow(h, 2);
                lblResultado.setText(String.format("Su resultado fue: %.2f", imc));
            } catch (NumberFormatException e) {
                Toast.makeText(getApplicationContext(), "Ingrese valores numéricos válidos", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
